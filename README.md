# flink-rdf-streaming-updater

This repo contains the blubber setup to build the docker image required to run the RDF Streaming Updater
in the WMF Kubernetes cluster.

## Content

The production image is based on the flink production base image with the presto s3 plugin installed and the
rdf streaming update jar included in the usrlib folder.

* `setup-flink.sh`: install the s3 plugin and download the rdf-streaming-updater jar to `urslib`
* `setup-test.sh`: additional preparation for the test image (currently copying the log4j files)
* `run-test.sh`: script to run the test
* `test-log4j-console.preparation`: log4j setup used by the test

## Test

There is a very naive test that just makes sure that the stream graph can be constructed and prints it into the log.
The `test-flink.sh` script takes care of running the test, we just assert that the stream graph is printed.

## Building and testing locally

```shell
docker buildx build  --target test --tag test_flink/devel:latest  -f .pipeline/blubber.yaml .
```

To run the test locally:
```shell
docker run --rm test_flink/devel ./test-flink.sh
```

Cleanup dev images:
```shell
docker image rm test_flink/devel
```
