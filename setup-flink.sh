#!/bin/bash
set -ex;

mkdir -p plugins/s3
cp opt/flink-s3-fs-presto-*.jar plugins/s3

/usr/bin/curl -s --fail --show-error https://archiva.wikimedia.org/repository/releases/org/wikidata/query/rdf/streaming-updater-producer/$JOB_VERSION/streaming-updater-producer-$JOB_VERSION-jar-with-dependencies.jar -o streaming-updater-producer.jar
rm setup-flink.sh

